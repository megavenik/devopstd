extends Area2D

export (int, 700, 4000, 100) var cpu_mhz
export (int, 256, 32768, 128) var memory_mb
export (int) var disk_size_gb
export (int, "HDD", "SSD") var disk_type # 1 for ssd, 0 for hdd
export (int) var bandwidth_mbit

export (int, "app", "db", "proxy", "backup") var server_type

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("enter")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
