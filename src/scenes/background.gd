extends Control

export (PackedScene) var Eth

var spawn_timer = 2
var last_spawn = 0

var VERTICAL_SPACING = 150
var HORISONTAL_SPACING = 90

func spawn_eth(pos, direction):
	var eth = Eth.instance()
	add_child(eth)
	eth.position = pos
	eth.rotation = direction
	eth.vel = Vector2(50, 0).rotated(direction)
	
func count_pos():
	var gamma = deg2rad(180-90) - PI/8
	var a = HORISONTAL_SPACING * (sin(PI/8)/sin(gamma))
	return a

# Called when the node enters the scene tree for the first time.
func _ready():
	# five rows
	for i in range(15):
		for x in range(10):
			spawn_eth(Vector2(-50 + x * HORISONTAL_SPACING, 50 + i * VERTICAL_SPACING - count_pos()*x), -PI/8)

#	yield(get_tree().create_timer(5.0),"timeout")
#	$background_animation.play("day-to-night")
#	yield(get_tree().create_timer(5.0),"timeout")
#	$background_animation.play("night-to-day")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if last_spawn <= spawn_timer:
		last_spawn += delta
	else:
		last_spawn = 0
		for i in range(15):
			spawn_eth(Vector2(-50,50 + i * VERTICAL_SPACING), -PI/8)
	
