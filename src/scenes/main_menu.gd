extends Node2D

export (PackedScene) var Server
export (int) var money = 5000
export (int) var seconds_to_day
var days_gone = 0

var time_of_day = true # true for day and false for night

# Called when the node enters the scene tree for the first time.
func _ready():
	$day_timer.wait_time = seconds_to_day
	$day_night_timer.wait_time = seconds_to_day/2

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	randomize()
	$hud.money = money
	$hud.days = days_gone

func start_game():
	days_gone = 0
	$hud/start.hide()
	$hud/game_info.show()
	$hud/buy_server.show()
	$day_timer.start()
	$day_night_timer.start()
	
func start_night():
	$background/background_animation.play("day-to-night")
	
func start_day():
	$background/background_animation.play("night-to-day")
	
func game_over():
	pass

func _on_day_timer_timeout():
	days_gone += 1

func _on_hud_start_game():
	start_game()

func _on_hud_create_server(cpu, mem, disk):
	print(cpu, mem, disk)
	var serv = Server.instance()
	add_child(serv)
	serv.position = Vector2(OS.window_size.x/2 + rand_range(-300, 300), OS.window_size.y/2 + rand_range(-300, 300))

func _on_day_night_timer_timeout():
	time_of_day = !time_of_day
	if time_of_day:
		start_day()
	else:
		start_night()
