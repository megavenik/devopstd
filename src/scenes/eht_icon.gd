extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var speed = 100
export var vel = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	vel = Vector2(speed, 100) # setting vel to desired direction
# Here would be code for KEY_UP, KEY_DOWN and KEY_LEFT
	self.translate(vel * delta) # changing node's position
	if self.position.x > get_viewport().size.x + 100 or self.position.y < -100:
		self.queue_free()
