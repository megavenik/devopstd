extends CanvasLayer

signal start_game

signal create_server(cpu, mem, disk)

export var DEBUG = false

export var days = 0
export var money = 0

#func update_data(d, m):
#	days = d
#	money = m
#	return true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$game_info.text = "Days gone: %d. Money: $%d" % [days, money]
	if DEBUG == true:
		$debug_info_label.text = "FPS: " + str(Performance.get_monitor(Performance.TIME_FPS)) + \
		"\n" + \
		"obj_count: " + str(Performance.get_monitor(Performance.OBJECT_COUNT)) + \
		"\n" + \
		"video memory used: " + str(Performance.get_monitor(Performance.RENDER_VIDEO_MEM_USED)/1024.0/1024.0) + "Mb"



func _on_start_button_up():
	$start.hide()
	emit_signal("start_game")


func _on_buy_server_button_up():
	emit_signal("create_server", 1800, 4096, 200)
